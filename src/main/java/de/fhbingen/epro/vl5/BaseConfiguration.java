package de.fhbingen.epro.vl5;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "de.fhbingen.epro.vl5.spring")
public class BaseConfiguration {
}
